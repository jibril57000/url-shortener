from flask import Flask, request, render_template, redirect #Import everything needed from flask
from Database import db

#Creating the app
app = Flask(__name__)
databse = db.Database("db")


#Creating all routes
@app.route("/" , methods=["GET"])
def home():
    return render_template("home.html")

@app.route("/", methods=["POST"])
def back_home():
    link = request.form.get("link")
    newLink = databse.add_link(link)
    return render_template("link.html",route=newLink, host=request.url_root)

@app.route("/<route>", methods=["GET"])
def check(route):
    if(route != "/"): return redirect("https://"+databse.get_link(route))
    
    
    

#Only run this in dev
# Can be dangerous

app.config['TEMPLATES_AUTO_RELOAD'] = True
app.run("localhost", 8282)