import os,sqlite3,secrets #for file system


class Database:
    def __init__(self, name):
        self.self = self
        self.name = name

        if(os.path.exists(self.name+".sqlite3")):print("[*] database already exists")
        else: 
            with sqlite3.connect(self.name+".sqlite3") as connect:
                c = connect.cursor()
                c.execute("CREATE TABLE links (id INTEGER PRIMARY KEY AUTOINCREMENT, route TEXT, link TEXT)")
            print("[$] created database")
    def query(self,sql,values, mode):
        # sourcery skip: merge-comparisons, merge-duplicate-blocks, remove-redundant-if
        with sqlite3.connect(self.name+".sqlite3") as connect:
            returned = []
            c = connect.cursor()
            if(mode == "add"):
                c.execute(sql,values)
                returned = c.fetchall()
            elif(mode == "select"):
                c.execute(sql,values)
                returned = c.fetchall()
            return returned 
    def add_link(self,link):
        route = secrets.token_urlsafe(16)
        self.query("INSERT INTO links VALUES (NULL,?,?)", (link,route), "add")
        return route
    def get_link(self, route):
        return self.query(f"SELECT * FROM links WHERE link='{route}'", "", "select")[0][1]
        


